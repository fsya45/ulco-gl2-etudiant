#pragma once

#include <string>

namespace masuperlib {
    std::string getVersion();
    int mul2(int n);
}
